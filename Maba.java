/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pendaftaranmaba;

import java.util.ArrayList;
import java.util.List;

public class Maba {
    public String nama;
    public String alamat;
    public String jk;
    public String tempatLahir;
    public String tanggalLahir;
    public String prodi;
    public String sekolahAsal;
    
    private static List<Maba> data;
    public static void add(Maba maba) {
        if (data == null) {
            data = new ArrayList<>();
        }
        data.add(maba);
    }
    
    public static List<Maba> all() {
        if (data == null) {
            data = new ArrayList<>();
        }
        
        return data;
    }
}
